#ifndef BOARD_H
#define BOARD_H
#include "Cell.h"
#include "PatternIterator.h"
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <ctime>
#include <algorithm>

using namespace std;

struct Coordinate;

typedef vector<vector<Cell>> BoardType;

class Board {
	public:
		Board(int);
		
		BoardType board; // [][]
		BoardType solution;
		
		int boardSize;
		int cellSize;
		

		// Begin Convenience Functions
		int numberOfCells();
		// End Convenience Functions

		int cellsHidden();
		int getValue(int, int);

		bool checkBox(BoardType, int, int, int);
		bool checkRow(BoardType, int, int);
		bool checkCol(BoardType, int, int);

		bool checkField(BoardType, int, int, int);
		bool checkField(int, int, int);

		bool isValid();

		Coordinate getEmptyCoord(BoardType);

		void generateSolvedBoard();
		pair<BoardType, bool> solve(BoardType);

		void seedBox(BoardType*, int, int);

	private:
		void fillEmpty();

};

struct Coordinate {
	public:
		Coordinate(int, int);
		int row, col;
};

#endif