#include "stdafx.h"
#include "PatternIterator.h"

PatternIter::PatternIter(int colInc, int rowInc) {
	this->incCol = colInc;
	this->incRow = rowInc;
	this->index = 0;
}

int PatternIter::nRow() {
	return (index += incRow) % 9;
}

int PatternIter::nCol() {
	return (index += incCol) % 9;
}

int PatternIter::getIndex() {
	return this->index;
}