#include "stdafx.h"
#include "Highscores.h"

void Highscores::addNewScore(Node* &root, int score) { // Adds a new score to the linked list
	Node* temp = new Node;
	temp->data = score;
	temp->next = NULL;

	if(!root) {
		root = temp;
	}else {
		Node* last = root;
		while(last->next) last = last->next;
		last->next = temp;
	}
}

void Highscores::sortList(Node **pp) { // Sorts the linked list using a bubble sort algorithm
    struct Node *p = *pp;
    *pp = nullptr;

    while (p)
    {
        struct Node **lhs = &p;
        struct Node **rhs = &p->next;
        bool swapped = false;
        while (*rhs) {
            if ((*rhs)->data < (*lhs)->data) {

                std::swap(*lhs, *rhs);
                std::swap((*lhs)->next, (*rhs)->next);
                lhs = &(*lhs)->next;
                swapped = true;
            }else {   
                lhs = rhs;
                rhs = &(*rhs)->next;
            }
        }

        *rhs = *pp;

      
        if (swapped) {
           
            *pp = *lhs;
            *lhs = nullptr;
        }else { 
            *pp = p;
            break;
        }
    }
}

void Highscores::printScoreList(Node *lst) { // Prints the linked list out 
	int count = 1;

    while (lst)
    {
        std::cout << ": " << lst->data << endl;
        lst = lst->next;
		count++;
    }
    std::cout << std::endl;
}

void Highscores::save() { // Saves the highscores linked list
	
	ofstream file;
	file.open("highscores.txt", fstream::out | fstream::trunc);

	this->sortList(&this->root);

	for(Node* n = root; n != nullptr; n = n->next) {
		file << n->data << endl;
	}


	file.close();
}

void Highscores::load() { // Loads the highscores linked list
	ifstream file;

	string line = "";

	file.open("highscores.txt");

	while(getline(file, line)) {
		this->addNewScore(this->root, stoi(line));
	}

	file.close();
}


