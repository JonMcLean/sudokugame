#ifndef PATTITER_H
#define PATTITER_H

class PatternIter {
	public:
		PatternIter(int colInc, int rowInc);

		int nCol();
		int nRow();

		int getIndex();

	private:
		int index;
		int incCol, incRow;
};

#endif