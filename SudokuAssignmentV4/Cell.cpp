#include "stdafx.h"
#include "Cell.h"

Cell::Cell() { }

bool Cell::isTried(int num) {

	if(num == 0) return true;
	if(this->tried.size() == 0) return false;

	for(int i = 0; i < this->tried.size(); i++) {
		if(num == i) {
			return true;
		}
	}

	return false;
}

void Cell::tryN(int n) {
	this->tried.push_back(n);
}

int Cell::numberOfTries() {
	return this->tried.size();
}

void Cell::setLocked(bool locked) {
	this->isLocked = locked;
}

bool Cell::locked() {
	return this->isLocked;
}