#ifndef UTILS_H
#define UTILS_H

#include "Board.h"
#include <Windows.h>
#include <conio.h>
#include <sstream>
#include <ctime>

using namespace std;

enum Difficulty;

class Utils {
	public:
		Difficulty gameDifficulty;
		BoardType board;
		BoardType solution; 
		Utils(BoardType, BoardType);
		void printASCII();
		int printSplash();
		void printBoard();
		void changeColor(int);
		BoardType takeSudokuInput();
		BoardType removeNums();
		double getTime();

	private:
		vector<int> randDone;
		int generateRandPos();
};

enum Difficulty {
	EASY,
	MED,
	HARD
};

#endif