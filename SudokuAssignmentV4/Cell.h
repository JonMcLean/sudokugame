#ifndef CELL_H
#define CELL_H
#include <string>
#include <vector>

using namespace std;

class Cell {
	public:
		Cell();
		int num;
		bool isFilled;

		bool isLocked;

		vector<int> tried;

		bool isTried(int);
		int numberOfTries();
		void tryN(int);

		bool locked();
		void setLocked(bool locked);
};

#endif