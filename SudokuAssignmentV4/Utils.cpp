#include "stdafx.h"
#include "Utils.h"

// Sets global values
Utils::Utils(BoardType board, BoardType solution) {
	this->board = board;
	this->solution = solution;
}

// Prints ASCII art
void Utils::printASCII() {
	cout << "   _________         .___      __          " << endl;
	cout << "  /   _____/__ __  __| _/____ |  | ____ __ " << endl;
	cout << "  \\_____  \\|  |  \\/ __ |/  _ \\|  |/ /  |  \\" << endl;
	cout << "  /        \\  |  / /_/ (  <_> )    <|  |  / " << endl;
	cout << " /_______  /____/\\____ |\\____/|__|_ \\____/ " << endl;
	cout << "         \\/           \\/           \\/    " << endl;
}

// Prints the splash screen
int Utils::printSplash() {
	system("cls");
	this->printASCII();
	cout << "\n";
	cout << "By Jon McLean" << endl << endl;
	cout << "This is a text based sudoku game with easy, medium, and hard difficulties. Additionally wihtin this game is saved and sorted highscores that allows for a bit of competition." << endl;
	_getch();

	_getch();

	system("cls");

	cout << "Select an option: " << endl;
	cout << "[1] Easy " << endl;
	cout << "[2] Medium " << endl;
	cout << "[3] Hard " << endl;
	cout << "[4] Highscores " << endl;

	int input = 0;

	cin >> input; // Gets the option input

	if(input != 0 && input <= 4) { // Determines if valid
		return input;
	}else {
		cout << "Invalid option" << endl; 
		_getch();
		return this->printSplash(); // If invalid re run the function to get input again.
	}
}

void Utils::printBoard() { // Prints board
	changeColor(12);
	cout << endl << "  0   1   2   3   4   5   6   7   8 " << endl; // Prints col numbering
	cout << " --- --- --- --- --- --- --- --- ---" << endl;
	for(int i = 0; i < board.size(); i++) {
		for(int j = 0; j < board[i].size(); j++) {
			if(j == 0) {
				changeColor(12);
				cout << "| ";
				changeColor(15);
				cout << board[i][j].num;
				changeColor(12);
				cout << " | ";
			}else if(j == board.size() -1) {
				changeColor(15);
				cout << board[i][j].num;
				changeColor(12);
				char let = 'a' + i; // Prints the number for row
				cout << " | " << let << endl;
			}
			else {
				changeColor(15);
				cout << board[i][j].num;
				changeColor(12);
				cout << " | ";
			}
		}
		cout << " --- --- --- --- --- --- --- --- ---" << endl;
	}

	changeColor(15);
}

void Utils::changeColor(int color) { // Changes the color of the text
	// red = 4
	// white = 15

	HANDLE hConsole;
	
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTextAttribute(hConsole, color);
}

BoardType Utils::takeSudokuInput() { // Gets the position input
	system("cls");
	this->printBoard();
	cout << "Enter the position you would like to enter the number into (e.g. 1a): " << endl;
	char pos[80];
	cin >> pos; // Takes the input in as a character array

	int num = 0;
	cout << "Enter the number you would like to enter: " << endl;
	cin >> num; // Takes in the input number
	cout << endl;

	int thing1 = pos[0]; 
	int thing2 = pos[1];

	int y;
	int x;

	y = pos[0] - '0'; // Determines the col coordinate through minusing of the value of the 0 character
	x = pos[1] - 'a'; // Determins the row coordinate through minusing of the value of the 'a' character


	if(x > 9 || y > 9) { // Invalid letter, number, or both
		cout << "Invalid position, please try again" << endl;
		system("pause");
		return this->takeSudokuInput();
	}

	if(board[x][y].locked()) { // Cell is locked
		cout << "Cell is locked, invalid position, please try again" << endl;
		system("pause");
		return this->takeSudokuInput();
	}

	//cout << conv << endl;

	pair<Coordinate, int> val = make_pair(Coordinate(x, y), num);

	board[val.first.row][val.first.col].num = val.second;
	return board; 
}

BoardType Utils::removeNums() { // Randomly removes numbers from the board

	int maxPerRow = 0;
	int minPerRow = 0;

	switch(this->gameDifficulty) { // Determines the range according to difficulty
		case EASY:
			maxPerRow = 1;
			minPerRow = 0;
			break;
		case MED:
			maxPerRow = 4;
			minPerRow = 3;
			break;
		case HARD:
			maxPerRow = 6;
			minPerRow = 5;
	}

	for(int i = 0; i < this->board.size(); i++) {

		int random =  (rand() % (maxPerRow - minPerRow)) + minPerRow; // Calculates how many numbers it should remove

		for(int j = 0; j <= random; j++) {
			int pos = this->generateRandPos(); // Randomly generates position
			this->board[i][pos].num = 0; // Removes number at random position
			this->board[i][pos].setLocked(false); 
		}
		randDone.clear();
	}

	return this->board;
}

int Utils::generateRandPos() { // Randomly generates the position without repeating
	int pos = rand() % 9;
	for(int i = 0; i < this->randDone.size(); i++) {
		if(pos == this->randDone[i]) {
			return generateRandPos();
		}
	}

	return pos;
}

double Utils::getTime() { // Gets the time
	
	time_t timer;
	struct tm y2k = {0};
	double seconds;

	// Sets paramaters for the time calculation e.g. which year to start with with 0 begin 2000
	y2k.tm_hour = 0;
	y2k.tm_min = 0;
	y2k.tm_sec = 0;
	y2k.tm_year = 100;
	y2k.tm_mon = 0;
	y2k.tm_mday = 1;

	time(&timer);
	seconds = difftime(timer, mktime(&y2k)); // Gets the time in seconds with parameters
	
	return seconds;
}


