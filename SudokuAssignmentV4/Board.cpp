#include "stdafx.h"
#include "Board.h"


// Intialized the board object
Board::Board(int size) {

	srand(time(NULL)); // Seeding random

	this->boardSize = size; // Setting board size
	this->cellSize = sqrt(size); // Setting cell size to be the sqaureroot of size

	this->fillEmpty(); // Fills the empty board
}

// Initializes the coordinate object
Coordinate::Coordinate(int row, int col) {
	this->row = row;
	this->col = col;
}

void Board::fillEmpty() { // Fils the empty board

	vector<vector<Cell>> temp; // Making temporary 2D vector to fill with zeroes.

	for(int i = 0; i < this->boardSize; i++) { // looping through 1st dimension
		vector<Cell> toFill; // creating 1st dimension element
		for(int j = 0; j < this->boardSize; j++) { // looping through second dimension
			Cell cell = Cell(); // Creating Cell
			cell.num = 0; // Giving number
			cell.isFilled = true; // Setting filled
			cell.isLocked = false; // Setting locked
			toFill.push_back(cell); // Adding the cell to the first dimension element
		}
		temp.push_back(toFill); // Adds the 1st dimension element to the temp vector
	}

	this->board = temp; // setting global board to be equal to the temp vector

	seedBox(&board,0,0); // Seeding the first 3x3 grid in the sudoku board
}

int Board::numberOfCells() {
	return pow(this->boardSize, 2); // Gets the number of total cells, in 9x9 this should be 81
}

int Board::getValue(int row, int col) {
	return this->board[row][col].num; // Gets the value at the coordinates
}

// Checks the number parameter against the numbers in the 3x3 grid currently and 
// And determines if it is valid
bool Board::checkBox(vector<vector<Cell>> board, int num, int row, int col) {
	int gx = floorf(row / 3) * 3;
	int gy = floorf(col / 3) * 3;

	for(int x = gx; x < gx + 3; x++) {
		for(int y = 0; y < gy + 3; y++) {
			if(board[gx][gy].num == num){
				return false;
			}
		}
	}

	return true;
}

// Checks the number parameter against the numbers in the row and determines if number is valid
bool Board::checkRow(vector<vector<Cell>> board, int num, int row) {
	for(int i = 0; i < board.size(); i++) {
		if((board[row][i].num == num)) {
			return false;
		}
	}

	return true;
}

// Checks number against column numbers and determines if valid
bool Board::checkCol(vector<vector<Cell>> board, int num, int col) {
	for(int i = 0; i < board.size(); i++) {
		if((board[i][col].num == num)) {
			return false;
		}
	}

	return true;
}

// Combines all three of the the above functions and returns an overall value that
// states whether the number is correct or not
bool Board::checkField(vector<vector<Cell>> board, int num, int row, int col) {

	bool checkbox = checkBox(board, num, row, col);
	bool checkrow = checkRow(board, num, row);
	bool checkcol = checkCol(board, num, col);

	return (checkrow && checkcol && checkbox);
}

// Runs checkField using the global board instead.
bool Board::checkField(int num, int row, int col){
	return checkField(board, num, row, col);
}

// Determines if the entirety of the board is filled, not zeroed, and valid.
bool Board::isValid() {
	for(int i = 0; i < this->boardSize; i++) {
		for(int j = 0; j < this->boardSize; j++) {
			if(this->board[i][j].isFilled && this->board[i][j].num != 0) {
				int val = this->board[i][j].num;
				bool valid = checkField(val, i, j);
				if(!valid) {
					return false;
				}
			}else {
				return false;
			}
		}
	}

	return true;
}

// Gets the next available empty cell coordinate
Coordinate Board::getEmptyCoord(vector<vector<Cell>> board) {
	for(int x = 0; x < board.size(); x++) {
		for(int y = 0; y < board.at(x).size(); y++) {
			if(board[x][y].num == 0) { // Checks if the cell is empty (which in this case means has a zero)
				return Coordinate(x, y); // Returns coordinate parameter.
			}
		}
	}

	return Coordinate(-1, -1); // Shows board is filled
}

/* Help from Ben McLean */
pair<BoardType, bool> Board::solve(BoardType lBoard) {

	Coordinate coord = this->getEmptyCoord(lBoard); // Gets the empty cell

	if(coord.row == -1 && coord.col == -1) { // Checks if board is filled
		return pair<BoardType, bool>(lBoard, true); // Returns the completed board and whether is succedded
	}

	for(int i = 1; i <= 9; i++) {
		if(checkField(lBoard, i, coord.row, coord.col)) {
			lBoard[coord.row][coord.col].num = i; // Sets the number at the coordinate
			lBoard[coord.row][coord.col].isLocked = true;

			pair<BoardType, bool> ret = solve(lBoard); // Re runs the function with the coordinate

			if(ret.second == true) { // Checks if board worked with number (didn't fail)
				return ret; // Returns board
			}

			
			lBoard[coord.row][coord.col].num = 0; // Removes the number if not valid
		}
	}

	return pair<BoardType, bool>(lBoard, false);
}

void Board::seedBox(vector<vector<Cell>>* board, int gx, int gy) {

	// Creates empty vector which is then filled with numbers from 1 to 9
	vector<int> nums;
	for(int i = 1; i <= 9; i++) {
		nums.push_back(i); 
	}

	random_shuffle(nums.begin(), nums.end()); // Vector is then shuffled

	int index = 0;

	for(int x = 0; x<3; x++) { // Loops through the rows in the 3x3 grid
		for(int y = 0; y < 3; y++) { // Loops through te cols in the 3x3 grid
			(*board)[x][y].num = nums[index++]; // Sets the number
		}
	}
}

