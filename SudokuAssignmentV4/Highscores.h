#ifndef HIGHSCORES_H
#define HIGHSCORES_H
#include <string>
#include <iostream>
#include <list>
#include <sstream>
#include <fstream>

using namespace std;

struct Node;

class Highscores {
	public:
		Node* root;

		void addNewScore(Node* &root, int);
		void sortList(Node **pp); 
		void printScoreList(Node *lst);

		void save();
		void load();
};

struct Node {
	int data;
	Node* next;

	Node(const int val, Node* n = 0) : data(val), next(n) {}
	Node(Node* n = 0) : next(n) {}
};

#endif