// SudokuAssignmentV4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Board.h"
#include "Utils.h"
#include "Highscores.h"
//#include "Cell.h"
#include <thread>
#include <random>

void solveBoard();
void printScreen(Board board);

Board b = Board(9);

int _tmain(int argc, _TCHAR* argv[])
{

	pair<BoardType, bool> ret = b.solve(b.board);
	b.board = ret.first;

	b.solution = b.board;

	printScreen(b);


	return 0;
}

void solveBoard() {
	pair<BoardType, bool> ret = b.solve(b.board);
	b.board = ret.first;
}

void printScreen(Board board) {
	Utils utils = Utils(board.board, board.solution);

	Highscores hs = Highscores();
	hs.load();

	int mainOpt = utils.printSplash();
	if(mainOpt == 1) {
		cout << "Hello World" << endl;
		utils.gameDifficulty = EASY;
		board.board = utils.removeNums();
		utils.printBoard();

		double begin = utils.getTime();

		while(!board.isValid()) {
			cout << "Meme" << endl;
			board.board = utils.takeSudokuInput();
		}

		cout << "Solved" << endl;

		double end = utils.getTime();

		double elapsed = end - begin;
		
		int score = (int)ceil(elapsed);

		hs.addNewScore(hs.root, score);
		hs.save();

		_getch();

	}else if(mainOpt == 2) {

		utils.gameDifficulty = MED;
		board.board = utils.removeNums();

		double begin = utils.getTime();

		while(!board.isValid()) {
			board.board = utils.takeSudokuInput();
		}

		cout << "Solved" << endl;

		double end = utils.getTime();

		double elapsed = end - begin;
		
		int score = (int)ceil(elapsed);

		hs.addNewScore(hs.root, score);
		hs.save();

		_getch();

	}else if(mainOpt == 3) {
		
		double begin = utils.getTime();

		utils.gameDifficulty = HARD;
		board.board = utils.removeNums();

		while(!board.isValid()) {
			board.board = utils.takeSudokuInput();
		}

		cout << "Solved" << endl;

		double end = utils.getTime();

		double elapsed = end - begin;
		
		int score = (int)ceil(elapsed);

		hs.addNewScore(hs.root, score);
		hs.save();

		_getch();


	}else { // Filled with example highscores
		system("cls");
		hs.printScoreList(hs.root);
		_getch();

	}

	printScreen(board);

	_getch();
}

 